package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	Port     int            `json:"port"`
	Env      string         `json:"env"`
	Pepper   string         `json:"pepper"`
	HMACKey  string         `json:"hmac_key"`
	Database PostgresConfig `json:"database"`
	Mailgun  MailgunConfig  `json:"mailgun"`
}

func (c Config) IsProd() bool {
	return c.Env == "prod"
}

type PostgresConfig struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

func DefaultPostgresConfig() PostgresConfig {
	return PostgresConfig{
		Host:     HOST,
		Port:     PORT,
		User:     USER,
		Password: PASSWORD,
		Name:     NAME,
	}
}

func (c PostgresConfig) Dialect() string {
	return "postgres"
}

func (c PostgresConfig) ConnectionInfo() string {
	if c.Password == "" {
		return fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable", c.Host, c.Port, c.User, c.Name)
	}
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=require", c.Host, c.Port, c.User, c.Password, c.Name)
}

type MailgunConfig struct {
	APIKey       string `json:"api_key"`
	PublicAPIKey string `json:"public_api_key"`
	Domain       string `json:"domain"`
}

func DefaultMailgunConfig() MailgunConfig {
	return MailgunConfig{
		APIKey:       os.Getenv("API_KEY"),
		PublicAPIKey: os.Getenv("PUBLIC_API_KEY"),
		Domain:       os.Getenv("DOMAIN"),
	}
}

func DefaultConfig() Config {
	return Config{
		Port:     3000,
		Env:      "prod",
		Pepper:   os.Getenv("PEPPER"),
		HMACKey:  os.Getenv("HMAC_KEY"),
		Database: DefaultPostgresConfig(),
		Mailgun:  DefaultMailgunConfig(),
	}
}

func LoadConfig(configReq bool) Config {
	f, err := os.Open(".config")
	if err != nil {
		if configReq {
			panic(err)
		}
		fmt.Println("Using the default config...")
		return DefaultConfig()
	}
	var c Config
	dec := json.NewDecoder(f)
	err = dec.Decode(&c)
	if err != nil {
		panic(err)
	}

	if c.Env == "prod" {
		return DefaultConfig()
	}

	fmt.Println("Successfully loaded .config")
	return c
}
